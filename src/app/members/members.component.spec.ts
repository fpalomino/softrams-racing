import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { MembersComponent } from './members.component';
import { Router } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { DebugElement } from '@angular/core';
import { AppService } from '../app.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';
import { MEMBERS_MOCK } from 'src/test-helpers/mocks';

describe('MembersComponent', () => {
  let component: MembersComponent;
  let fixture: ComponentFixture<MembersComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MembersComponent],
      imports: [HttpClientModule, RouterModule],
      providers: [
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }
      ]
    }).compileComponents();
  }));

  beforeEach(inject([AppService], (service: AppService) => {
    spyOn(service, 'getMembers').and.returnValue(of(MEMBERS_MOCK));
    fixture = TestBed.createComponent(MembersComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should go to add member', inject([Router], (router: Router) => {
    spyOn(component, 'goToAddMemberForm').and.callThrough();

    const addMemberButton = debugElement.nativeElement.querySelector('#addMemberButton');
    addMemberButton.click();

    expect(component.goToAddMemberForm).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/member-details']);
  }));

  it('should go to member details', inject([Router], (router: Router) => {
    spyOn(component, 'editMemberByID').and.callThrough();

    const editBtn = debugElement.query(By.css('.edit')).nativeElement;
    editBtn.click();
    expect(component.editMemberByID).toHaveBeenCalled();
    expect(router.navigate).toHaveBeenCalledWith(['/member-details', { id: 1 }]);
  }));

  it('should go to member details', inject([AppService, Router],
    (service: AppService, router: Router) => {
      spyOn(component, 'deleteMemberById').and.callThrough();
      spyOn(service, 'deleteMember').and.returnValue(of('success'));

      const deleteBtn = debugElement.query(By.css('.delete')).nativeElement;
      deleteBtn.click();
      expect(component.deleteMemberById).toHaveBeenCalled();
    }));

});
