import { async, ComponentFixture, inject, TestBed } from '@angular/core/testing';
import { BannerComponent } from './banner.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';

describe('BannerComponent', () => {
  let component: BannerComponent;
  let fixture: ComponentFixture<BannerComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [BannerComponent],
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [{
        provide: Router,
        useClass: class {
          navigate = jasmine.createSpy('navigate');
        }
      }]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BannerComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should logout', inject([AppService], (service: AppService) => {
    spyOn(component, 'logout').and.callThrough();
    service.setUsername('John');
    fixture.detectChanges();
    const logout = debugElement.query(By.css('.logout')).nativeElement;
    logout.click();
    expect(component.logout).toHaveBeenCalled();
  }));

});
