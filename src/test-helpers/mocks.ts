import { FormControl, FormGroup } from '@angular/forms';

export const MEMBERS_MOCK = [
    {
      'id': 1,
      'firstName': 'John',
      'lastName': 'Doe',
      'jobTitle': 'Driver',
      'team': 'Formula 1 - Car 77',
      'status': 'Active'
    },
    {
      'id': 2,
      'firstName': 'Alex',
      'lastName': 'Sainz',
      'jobTitle': 'Driver',
      'team': 'Formula 1 - Car 78',
      'status': 'Active'
    },
    {
      'id': 3,
      'firstName': 'Jeb',
      'lastName': 'Jackson',
      'jobTitle': 'Reserve Driver',
      'team': 'Formula 1 - Car 77',
      'status': 'Inactive'
    }
  ];

  export const TEAMS_MOCK = [
    {
      'id': 1,
      'teamName': 'Formula 1 - Car 77'
    },
    {
      'id': 2,
      'teamName': 'Formula 1 - Car 78'
    },
    {
      'id': 3,
      'teamName': 'Formula 2 - Car 54'
    }
  ];

  export const LOGIN_FORM_MOCK = new FormGroup({
    username: new FormControl('john'),
    password: new FormControl('123456')
  });
