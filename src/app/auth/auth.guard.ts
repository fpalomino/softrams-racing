import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AUTH_USER_TOKEN } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _router: Router) { }

  canActivate(): boolean {
    const token = JSON.parse(localStorage.getItem(AUTH_USER_TOKEN));
    if (token && token.access_token) {
      return true;
    }
    this._router.navigate(['login']);
  }

}
