import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { AUTH_USER_TOKEN } from '../constants';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {
  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() { }

  logout() {
    this.appService.username = '';
    localStorage.removeItem('username');
    localStorage.removeItem(AUTH_USER_TOKEN);
    this.router.navigate(['/login']);
  }
}
