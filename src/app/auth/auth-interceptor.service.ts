import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AUTH_USER_TOKEN } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(private _router: Router) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.includes('auth/login')) {
      return next.handle(req);
    }

    const token = JSON.parse(localStorage.getItem(AUTH_USER_TOKEN));
    if (token && token.access_token) {
      const authReq = req
        .clone({ setHeaders: { Authorization: ['Bearer', token.access_token].join(' ') } });
      return next.handle(authReq);
    } else {
      localStorage.removeItem(AUTH_USER_TOKEN);
      this._router.navigate(['login']);
    }
  }
}
