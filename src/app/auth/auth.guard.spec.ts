import { TestBed, async, inject } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AUTH_USER_TOKEN } from '../constants';
import { AuthGuard } from './auth.guard';

describe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuard,
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        }]
    });
  });

  it('should load', inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  describe('canActivate', () => {

    it('should return true', inject([AuthGuard],
      (guard: AuthGuard) => {
        localStorage.setItem(AUTH_USER_TOKEN, '{\"access_token\":\"3423dsfs\"}');
        expect(guard.canActivate()).toBeTruthy();
        localStorage.removeItem(AUTH_USER_TOKEN);
      }));

    it('should return navigate', inject([AuthGuard, Router],
      (guard: AuthGuard, router: Router) => {
        guard.canActivate();
        expect(router.navigate).toHaveBeenCalledWith(['login']);
      }));
  });

});
