import { Component, OnInit, OnChanges, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

// This interface may be useful in the times ahead...
interface Member {
  id: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  team: string;
  status: string;
}

@Component({
  selector: 'app-member-details',
  templateUrl: './member-details.component.html',
  styleUrls: ['./member-details.component.css']
})
export class MemberDetailsComponent implements OnInit, OnDestroy {

  private _subscriptions: Subscription[] = [];

  memberModel: Member;
  memberForm: FormGroup;
  submitted = false;
  alertType: String;
  alertMessage: String;
  teams = [];
  memberId: string;

  constructor(private fb: FormBuilder, private appService: AppService,
    private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
    this.memberId = this.route.snapshot.paramMap.get('id');
    this.memberForm = this.fb.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      jobTitle: new FormControl('', Validators.required),
      team: new FormControl('', Validators.required),
      status: new FormControl('', Validators.required)
    });
    this.getTeams();
  }

  getTeams() {
    this._subscriptions.push(this.appService.getTeams().subscribe(teams => {
      this.teams = teams;
      this.getMemberDetails(this.memberId);
    }));
  }

  getMemberDetails(id: string) {
    this._subscriptions.push(this.appService.getMember(id).subscribe(response => {
      const member = response[0];
      if (member) {
        this.memberForm = this.fb.group({
          firstName: new FormControl(member.firstName, Validators.required),
          lastName: new FormControl(member.lastName, Validators.required),
          jobTitle: new FormControl(member.jobTitle, Validators.required),
          team: new FormControl(member.team, Validators.required),
          status: new FormControl(member.status, Validators.required)
        });
      }
    }));
  }

  // TODO: Add member to members
  onSubmit(form: FormGroup) {
    this.memberModel = form.value;
    this.memberModel.id = this.memberId;
    this._subscriptions.push(this.appService.addMember(this.memberModel).subscribe(
      () => {
        this.router.navigate(['/members']);
      }
    ));
  }

  cancel() {
    this.router.navigate(['/members']);
  }

  get memberFormControl() {
    return this.memberForm.controls;
  }

  ngOnDestroy() {
    this._subscriptions.forEach(sub => sub.unsubscribe());
  }
}
