import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { LoginComponent } from './login.component';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { DebugElement } from '@angular/core';
import { AppService } from '../app.service';
import { By } from '@angular/platform-browser';
import { of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let debugElement: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [ReactiveFormsModule, RouterModule, HttpClientModule],
      providers: [
        {
          provide: Router,
          useClass: class {
            navigate = jasmine.createSpy('navigate');
          }
        },
        HttpClient
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should login', inject([AppService], (service: AppService) => {
    spyOn(component, 'login').and.callThrough();
    spyOn(service, 'login').and.returnValue(of({ 'status': 200 }));

    const username = debugElement.nativeElement.querySelector('#username');
    const password = debugElement.nativeElement.querySelector('#password');
    username.value = 'john';
    password.value = '123456';
    fixture.detectChanges();
    const form = debugElement.query(By.css('form'));
    form.triggerEventHandler('ngSubmit', null);
    expect(component.login).toHaveBeenCalled();
    expect(component.incorrectUser).toBe(false);
  }));

  it('should be bad login', inject([AppService], (service: AppService) => {
    spyOn(component, 'login').and.callThrough();
    spyOn(service, 'login').and.returnValue(of({ 'status': 401 }));

    const username = debugElement.nativeElement.querySelector('#username');
    const password = debugElement.nativeElement.querySelector('#password');
    username.value = 'john';
    password.value = '123456';
    fixture.detectChanges();
    const form = debugElement.query(By.css('form'));
    form.triggerEventHandler('ngSubmit', null);
    expect(component.login).toHaveBeenCalled();
    expect(component.incorrectUser).toBe(true);
  }));
});
