import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.css']
})
export class MembersComponent implements OnInit, OnDestroy {

  private _subscriptions: Subscription[] = [];
  members = [];

  constructor(public appService: AppService, private router: Router) { }

  ngOnInit() {
    this._subscriptions.push(this.appService.getMembers()
      .subscribe(members => (this.members = members)));
  }

  goToAddMemberForm() {
    this.router.navigate(['/member-details']);
  }

  editMemberByID(id: number) {
    this.router.navigate(['/member-details', { id: id }]);
  }

  deleteMemberById(id: number) {
    this._subscriptions.push(this.appService.deleteMember(id).subscribe(
      () => {
        this.members = this.members.filter(member => member.id !== id);
      }
    ));
  }

  ngOnDestroy() {
    this._subscriptions.forEach(sub => sub.unsubscribe());
  }
}
