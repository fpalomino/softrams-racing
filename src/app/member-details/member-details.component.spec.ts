import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { MemberDetailsComponent } from './member-details.component';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppService } from '../app.service';
import { of } from 'rxjs';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';
import { MEMBERS_MOCK, TEAMS_MOCK } from 'src/test-helpers/mocks';

// Bonus points!
describe('MemberDetailsComponent', () => {
  let component: MemberDetailsComponent;
  let fixture: ComponentFixture<MemberDetailsComponent>;
  let debugElement: DebugElement;
  const routes: Routes = [
    { path: 'members', component: MemberDetailsComponent }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MemberDetailsComponent],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
        RouterModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        HttpClient,
        FormBuilder
      ]
    }).compileComponents();
  }));

  beforeEach(inject([AppService], (service: AppService) => {
    spyOn(service, 'getTeams').and.returnValue(of([TEAMS_MOCK]));
    fixture = TestBed.createComponent(MemberDetailsComponent);
    debugElement = fixture.debugElement;
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should load with Member', inject([AppService], (service: AppService) => {
    spyOn(service, 'getMember').and.returnValue(of([MEMBERS_MOCK[0]]));
    fixture = TestBed.createComponent(MemberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  }));

  it('should load with no Member', inject([AppService], (service: AppService) => {
    spyOn(service, 'getMember').and.returnValue(of([]));
    fixture = TestBed.createComponent(MemberDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  }));

  it('should submit member form', inject([AppService, Router],
    (service: AppService, router: Router) => {
      spyOn(component, 'onSubmit').and.callThrough();
      spyOn(service, 'addMember').and.returnValue(of({ 'status': 200 }));
      spyOn(router, 'navigate');

      component.memberForm.controls['firstName'].setValue('John');
      component.memberForm.controls['lastName'].setValue('Doe');
      component.memberForm.controls['jobTitle'].setValue('Driver');
      component.memberForm.controls['team'].setValue('Formula 1 - Car 77');
      component.memberForm.controls['status'].setValue('Active');
      fixture.detectChanges();
      const form = debugElement.query(By.css('form'));
      form.triggerEventHandler('ngSubmit', null);

      expect(component.onSubmit).toHaveBeenCalled();
      expect(component.memberModel.firstName).toEqual('John');
      expect(component.memberModel.lastName).toEqual('Doe');
      expect(component.memberModel.jobTitle).toEqual('Driver');
      expect(component.memberModel.team).toEqual('Formula 1 - Car 77');
      expect(component.memberModel.status).toEqual('Active');
      expect(router.navigate).toHaveBeenCalledWith(['/members']);
    }));

  it('should cancel form', inject([Router],
    (router: Router) => {
      spyOn(component, 'cancel').and.callThrough();
      spyOn(router, 'navigate');

      const cancelBtn = debugElement.nativeElement.querySelector('#cancel');
      cancelBtn.click();
      expect(component.cancel).toHaveBeenCalled();
      expect(router.navigate).toHaveBeenCalledWith(['/members']);
    }));

});
