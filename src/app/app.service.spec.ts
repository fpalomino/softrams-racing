import { TestBed, inject } from '@angular/core/testing';
import { AppService } from './app.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Router } from '@angular/router';
import { MEMBERS_MOCK, LOGIN_FORM_MOCK, TEAMS_MOCK } from 'src/test-helpers/mocks';
import { API } from './constants';

describe('AppService', () => {
  let httpTestingController: HttpTestingController;
  let appService: AppService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{
        provide: Router,
        useClass: class {
          navigate = jasmine.createSpy('navigate');
        }
      }]
    });
    httpTestingController = TestBed.get(HttpTestingController);
  });

  beforeEach(inject(
    [AppService],
    (service: AppService) => {
      appService = service;
    }
  ));

  it('should be created', () => {
    expect(appService).toBeTruthy();
  });

  it('should be get Members', () => {
    let result;
    appService.getMembers().subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/members`
    });
    req.flush(MEMBERS_MOCK);
    expect(result[0].firstName).toEqual(MEMBERS_MOCK[0].firstName);
    expect(result[1].lastName).toEqual(MEMBERS_MOCK[1].lastName);
    expect(result[2].jobTitle).toEqual(MEMBERS_MOCK[2].jobTitle);
  });

  it('should be get Member index 0', () => {
    let result;
    appService.getMember('1').subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/member?id=1`
    });
    req.flush(MEMBERS_MOCK[0]);
    expect(result.firstName).toEqual(MEMBERS_MOCK[0].firstName);
    expect(result.lastName).toEqual(MEMBERS_MOCK[0].lastName);
    expect(result.jobTitle).toEqual(MEMBERS_MOCK[0].jobTitle);
  });

  it('should delete Member', () => {
    let result;
    appService.deleteMember(1).subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'DELETE',
      url: `${API}/deleteMember?id=1`
    });
    req.flush({ message: 'success' });
    expect(result.message).toEqual('success');
  });

  it('should login', () => {
    let result;

    appService.login(LOGIN_FORM_MOCK).subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${API}/auth/login`
    });
    req.flush({ message: 'success' });
    expect(result.message).toEqual('success');
  });

  it('should handle login error', () => {
    let error;
    appService.login(LOGIN_FORM_MOCK).subscribe(err => {
      error = err;
    });
    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${API}/auth/login`
    });
    req.flush('Something went wrong', {
      status: 401,
      statusText: 'Unauthorized'
    });
    expect(error['status']).toEqual(401);
    expect(error['statusText']).toEqual('Unauthorized');
  });

  it('should add Member', () => {
    let result;
    appService.addMember(MEMBERS_MOCK[0]).subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'POST',
      url: `${API}/addMember`
    });
    req.flush({ message: 'success' });
    expect(result.message).toEqual('success');
  });

  it('should get Teams', () => {
    let result;
    appService.getTeams().subscribe(res => {
      result = res;
    });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/teams`
    });
    req.flush(TEAMS_MOCK);
    expect(result[0].teamName).toEqual(TEAMS_MOCK[0].teamName);
    expect(result[1].teamName).toEqual(TEAMS_MOCK[1].teamName);
    expect(result[2].teamName).toEqual(TEAMS_MOCK[2].teamName);
  });

  it('should handle ErrorEvent', () => {
    spyOn(console, 'error').and.callThrough();
    appService.getTeams().subscribe(() => { });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/teams`
    });
    const message = 'Error';
    req.error(new ErrorEvent('Error Event', { message }));
    expect(console.error)
      .toHaveBeenCalledWith('An error occurred:', 'Error');
  });

  it('should handle 401 error', () => {
    spyOn(console, 'error').and.callThrough();
    let result;
    appService.getTeams().subscribe(null, err => {
      result = err;
    });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/teams`
    });
    req.flush('Something went wrong', {
      status: 401,
      statusText: 'Unauthorized'
    });
    expect(result).not.toBeNull();
    expect(console.error)
      .toHaveBeenCalledWith('Backend returned code 401, body was: Something went wrong');
  });

  it('should handle 404 error', () => {
    spyOn(console, 'error').and.callThrough();
    let result;
    appService.getTeams().subscribe(null, err => {
      result = err;
    });
    const req = httpTestingController.expectOne({
      method: 'GET',
      url: `${API}/teams`
    });
    req.flush('Something went wrong', {
      status: 404,
      statusText: 'Page Not Found'
    });
    expect(result).not.toBeNull();
    expect(console.error)
      .toHaveBeenCalledWith('Backend returned code 404, body was: Something went wrong');
  });

  it('should set name', () => {
    appService.setUsername('John');
    expect(appService.username).toEqual('John');
  });

});
