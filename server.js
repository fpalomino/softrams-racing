const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const helmet = require('helmet');
var hsts = require('hsts');
const path = require('path');
var xssFilter = require('x-xss-protection');
var nosniff = require('dont-sniff-mimetype');
const request = require('request');

var jwt = require('jsonwebtoken');
const fs = require('fs');
const userdb = JSON.parse(fs.readFileSync('./usersDB.json', 'UTF-8'));
const SECRET_KEY = '123_Secret!';
const DB_API = 'http://localhost:3000';

const app = express();

app.use(cors());
app.use(express.static('assets'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');
app.use(xssFilter());
app.use(nosniff());
app.set('etag', false);
app.use(
  helmet({
    noCache: true
  })
);
app.use(
  hsts({
    maxAge: 15552000 // 180 days in seconds
  })
);

app.use(
  express.static(path.join(__dirname, 'dist/softrams-racing'), {
    etag: false
  })
);

app.use(/^(?!\/api\/auth).*$/, (req, res, next) => {

  if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
    const status = 401;
    const message = 'Authorization header not valid';
    res.status(status).json({ status, message });
    return;
  }

  try {
    const token = req.headers.authorization.split(' ')[1];
    const { name } = jwt.verify(token, SECRET_KEY,
      (err, decode) => decode !== undefined ? decode : err);

    if (name === 'JsonWebTokenError') {
      throw new Error('Invalid Token');
    }
    next();
  } catch (err) {
    const status = 401;
    const message = 'Access token is not valid';
    res.status(status).json({ status, message });
  }

});

app.post('/api/auth/login', (req, res) => {
  let status = 200;
  const { username, password } = req.body;
  const user = login({ username, password });
  if (!user) {
    status = 401;
    const message = 'Incorrect username or password';
    res.status(status).json({ status, message });
    return;
  }
  const access_token = jwt.sign(user, SECRET_KEY, { expiresIn: '1h' });
  res.status(200).json({ status, access_token });
});

app.get('/api/members', (req, res) => {
  request(DB_API + '/members', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

app.get('/api/member', (req, res) => {
  request(DB_API + '/members?id=' + req.query.id, (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// TODO: Dropdown!
app.get('/api/teams', (req, res) => {
  request(DB_API + '/teams', (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

// Submit Form!
app.post('/api/addMember', (req, res) => {
  const { id, firstName, lastName, jobTitle, team, status } = req.body;

  if (!firstName || !lastName || !jobTitle || !team || !status) {
    const status = 400;
    const message = 'Missing required fields';
    res.status(400).json({ status, message });
  } else if (id) {
    const options = {
      uri: DB_API + '/members/' + id,
      json: { firstName, lastName, jobTitle, team, status }
    };
    request.put(options, (err, response, body) => {
      if (response.statusCode <= 500) {
        res.send(body);
      }
    });
  } else {
    console.log(req.body);
    const options = {
      uri: DB_API + '/members/',
      json: { firstName, lastName, jobTitle, team, status }
    };
    request.post(options, (err, response, body) => {
      if (response.statusCode <= 500) {
        res.send(body);
      }
    });
  }
});

app.delete('/api/deleteMember', (req, res) => {
  request.delete(DB_API + '/members/' + req.query.id, (err, response, body) => {
    if (response.statusCode <= 500) {
      res.send(body);
    }
  });
});

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/softrams-racing/index.html'));
});

app.listen('8000', () => {
  console.log('Vrrrum Vrrrum! Server starting!');
});

function login({ username, password }) {
  return userdb.users.find(user => user.username === username && user.password === password);
}
