import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { API } from './constants';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  username: string;

  constructor(private http: HttpClient, private _router: Router) { }

  // Returns all members
  getMembers() {
    return this.http
      .get(`${API}/members`)
      .pipe(catchError(this.handleError));
  }

  getMember(id: string) {
    return this.http
      .get(`${API}/member?id=${id}`)
      .pipe(catchError(this.handleError));
  }

  deleteMember(id: number) {
    return this.http
      .delete(`${API}/deleteMember?id=${id}`)
      .pipe(catchError(this.handleError));
  }

  login(form: FormGroup) {
    return this.http
      .post(`${API}/auth/login`, form.value)
      .pipe(catchError(this.handleLoginError));
  }

  setUsername(name: string): void {
    this.username = name;
  }

  addMember(member) {
    return this.http
      .post(`${API}/addMember`, member)
      .pipe(catchError(this.handleLoginError));
  }

  getTeams() {
    return this.http
      .get(`${API}/teams`)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
      if (error.status === 401) {
        localStorage.clear();
        this._router.navigate(['/login']);
      }
    }
    return [];
  }

  private handleLoginError(error: HttpErrorResponse) {
    return [error];
  }
}
