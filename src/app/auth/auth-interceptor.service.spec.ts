import { HttpRequest } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { AUTH_USER_TOKEN } from '../constants';

import { AuthInterceptorService } from './auth-interceptor.service';

const mockReq = {
  body: null, headers: null,
  reportProgress: null, withCredentials: null, responseType: null, method: null,
  params: null, urlWithParams: null, serializeBody: null, detectContentTypeHeader: null, clone: null,
  url: null
};
const err: any = { status: 500 };
const mockNext: any = {
  handle: (request: HttpRequest<any>) => ({
    catch: (callback: Function) => callback(err)
  })
};

describe('AuthInterceptorService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: Router,
        useClass: class {
          navigate = jasmine.createSpy('navigate');
        }
      }]
  }));

  it('should be created', () => {
    const service: AuthInterceptorService = TestBed.get(AuthInterceptorService);
    expect(service).toBeTruthy();
  });

  describe('Intercept', () => {
    it('should call next.handle()', inject([AuthInterceptorService],
      (service: AuthInterceptorService) => {
        mockReq.url = 'auth/login';
        spyOn(mockNext, 'handle');
        service.intercept(mockReq, mockNext);
        expect(mockNext.handle).toHaveBeenCalled();
      }));

    it('should call next.handle() with token', inject([AuthInterceptorService],
      (service: AuthInterceptorService) => {
        const token = '{\"access_token\":\"3423dsfs\"}';
        mockReq.url = 'get';
        mockReq.clone = (): any => token;
        localStorage.setItem(AUTH_USER_TOKEN, token);
        spyOn(mockNext, 'handle');
        service.intercept(mockReq, mockNext);

        expect(mockNext.handle).toHaveBeenCalledWith(token);

        localStorage.removeItem(AUTH_USER_TOKEN);
      }));

    it('should call redirect to login', inject([AuthInterceptorService, Router],
      (service: AuthInterceptorService, router: Router) => {
        mockReq.url = 'get';
        service.intercept(mockReq, mockNext);
        expect(router.navigate).toHaveBeenCalledWith(['login']);
      }));
  });
});
