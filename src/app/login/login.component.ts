import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AppService } from '../app.service';
import { AUTH_USER_TOKEN } from '../constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  private _subscriptions: Subscription[] = [];
  incorrectUser: boolean;

  loginForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private appService: AppService) {
  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    });
  }

  login() {
    this.incorrectUser = false;
    this._subscriptions.push(this.appService.login(this.loginForm).subscribe(
      res => {
        if (res['status'] === 200) {
          localStorage.setItem('username', this.loginForm.value.username);
          this.appService.setUsername(this.loginForm.value.username);
          localStorage.setItem(AUTH_USER_TOKEN, JSON.stringify(res));
          this.router.navigate(['/members']);
        } else {
          this.incorrectUser = true;
        }
      }
    ));
  }

  ngOnDestroy() {
    this._subscriptions.forEach(sub => sub.unsubscribe());
  }

}
